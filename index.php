<?php

require 'animal.php';
require 'frog.php';

$sheep = new Animal;
$ape = new Animal;
$frog = new Animal;

$sheep->set_name ('Shaun');
echo 'Namanya ' . $sheep->get_name();// "shaun"
echo "<br>";

$sheep->set_legs (2);
echo 'Kakinya ' . $sheep->get_legs();
echo "<br>"; // 2


$sheep->set_cold_blooded ('false');
echo 'Blooded   ' . $sheep->get_cold_blooded(); // false
echo "<br>";
echo "<br>";

$ape->set_name('Kera Sakti');
echo 'Namanya Yang ke 2 ' . $ape->get_name();
echo "<br>";

$ape->set_yell('Auoooo');
echo 'Bunyinya ' . $ape->get_name();
echo "<br>";
echo "<br>";

$frog->set_name('Buduk');
echo 'Namanya Yang ke 3 ' . $frog->get_name();
echo "<br>";

$ape->set_jump('Hop Hop');
echo 'Bunyinya ' . $frog->get_name();
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>