<?php

class Animal {

    public $name;
    public $legs;
    public $cold_blooded;
    public $yell;

    public function set_name ($name1){
        $this->name = $name1;
    }
    public function get_name (){
        return $this->name;
    }
    
    public function set_legs ($legs1){
        $this->legs = $legs1;
    }
    public function get_legs (){
        return $this->legs;
    }

    public function set_cold_blooded ($cold_blooded1){
        $this->cold_blooded = $cold_blooded1;
    }
    public function get_cold_blooded (){
        return $this->cold_blooded;
    }

    
    public function set_yell ($yell1){
        $this->yell = $yell1;
    }
    public function get_yell (){
        return $this->yell;
    }

    public function set_jump ($jump1){
        $this->jump = $jump1;
    }
    public function get_jump (){
        return $this->jump;
    }
}




?>